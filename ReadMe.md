# Create TYPO3-9 with composer
* git clone this Repository
* composer install
* copy .env-example to .env
* Import Database
  * if you have it: ddev import-db --src=PATH/DB.sql
  * if you create a new one: 
    * touch web/FIRST_INSTALL
    * go to YOUR_HOST_ADDRESS/typo3/install.php in browser
* rm -rf .git && git init && git add . && git commit -m "First commit"