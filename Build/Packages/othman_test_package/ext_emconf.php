<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'OTHMAN TYPO3 Site Package',
    'description' => 'An example site package',
    'category' => 'TYPO3 Console',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Majd Othman',
    'author_email' => 'majd.os.sy@hotmail.com',
    'author_company' => '',
    'version' => '0.1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.7.0-9.7.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
