<?php

/**
 * @ex: T3__SYS__sitename="TYPO3 Composer Site" -> $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = 'TYPO3 Composer Site';
 * @link https://github.com/helhum/config-loader Configuration Loader.
 */

/* Load environment variables */
$configLoader = new \Helhum\ConfigLoader\ConfigurationLoader(
    [
        new \Helhum\ConfigLoader\Reader\EnvironmentReader('T3'),
    ]
);

/* Write env variables into TYPO3_CONF_VARS */
$GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
    $GLOBALS['TYPO3_CONF_VARS'],
    $configLoader->load()
);
